# Setup crc locally

```bash
# Setup CRC
crc setup

# Give CRC some extra resources
crc config set cpus 8
crc config set memory 12288

# Start crc
crc start

# Get your secret here
https://cloud.redhat.com/openshift/install/crc/installer-provisioned

# Open crc in your browser
crc console

# TO GET YOUR CREDENTIONALS
crc console --credentials

# Login via developer
oc login -u developer -p developer https://api.crc.testing:6443

# As admin add a new project where we will deploy our applications
oc new-project crv4all

# Create a secret from your private key used for the SCM
# For example: oc create secret generic scmsecret --from-file=ssh-privatekey=/Users/pepijn/.ssh/id_rsa
oc create secret generic scmsecret --from-file=ssh-privatekey=<PRIVATE_KEY_LOCATION>
oc secrets link builder scmsecret

# As admin add a new ci-cd project
oc new-project ci-cd

# Create a secret from your private key used for the SCM
# For example: oc create secret generic scmsecret --from-file=ssh-privatekey=/Users/pepijn/.ssh/id_rsa
oc create secret generic scmsecret --from-file=ssh-privatekey=<PRIVATE_KEY_LOCATION>
oc secrets link builder scmsecret

# Spin up a Jenkins app
oc new-app --template=jenkins-persistent

# Give Jenkins permission to start builds / deployments in the crv4all project
oc adm policy add-role-to-user edit system:serviceaccount:ci-cd:jenkins -n crv4all

#Login admin
oc login -u kubeadmin -p PWD https://api.crc.testing:6443

# AS ADMIN, make sure Jenkins is allowed to create projects and apps for arquillian tests
oc adm policy add-cluster-role-to-user self-provisioner system:serviceaccount:ci-cd:jenkins

# Switch to crv4all project
oc project crv4all

# Create operator group
oc create -f operator/operatorgroup.yaml 

# Create amq operator
oc create -f amq/operator.yaml 

# Use operator to start an AMQ instance
oc create -f amq/broker.yaml

# Use operator to start a scaledown for clustering
oc create -f amq/scaledown.yaml

# Create an queue
oc create -f amq/queues/domainEventsQueue.yaml

# Login via developer
oc login -u developer -p developer https://api.crc.testing:6443

# Switch to crv4all project
oc project crv4all

# Create all build configuration per app in the ci-cd project
# You can find an example the buildconfig.yaml
oc create -f buildconfig.yaml

# Create a configmap
oc create -f configmap.yaml

# Create
oc create -f app.yaml 
```


